package de.bht.fpa.mail.s750042.messages;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import de.bht.fpa.mail.common.model.Message;
import de.bht.fpa.mail.s750042.control.ClientManager;
import de.bht.fpa.mail.s750042.events.DirectoryChangedEvent;
import de.bht.fpa.mail.s750042.navigation.DirectoryItem;
import de.bht.fpa.mail.s750042.toolbox.Search;
import de.bht.fpa.mail.s750042.toolbox.TableComparator;

public class MessagesView extends ViewPart implements Observer {

	public static final String ID = "de.bht.fpa.mail.s750042.messages.MessagesView";

	private TableViewer tableViewer;
	private TableComparator comparator;
	private Search filter;
	private List<Message> messageList;
	private DirectoryItem currentFolder;

	@Override
	public void createPartControl(Composite parent) {
		setParentLayout(parent);
		filter = createFindField(parent);
		tableViewer = createTableViewer(parent);
		configureTable(tableViewer);
		ClientManager.getInstance().addObserver(this);
	}

	private void setParentLayout(Composite parent) {
		GridLayout layout = new GridLayout(2, false);
		parent.setLayout(layout);
	}
	
	private void configureTable(TableViewer tableViewer) {
		int[] bounds = { 150, 150, 150, 15 };
		String[] titles = { "Von", "Betreff", "Erhalten", "Wichtig" };
		
		final Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setVisible(true);

		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Message) element).getSender().getFirstName() + " "
						+ ((Message) element).getSender().getFirstName();
			}
		});

		col = createTableViewerColumn(titles[1], bounds[1], 1);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Message) element).getSubject();
			}
		});

		col = createTableViewerColumn(titles[2], bounds[2], 2);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Message msg = (Message) element;
				return msg.getReceived().toString();
			}
		});

		col = createTableViewerColumn(titles[3], bounds[3], 3);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Message msg = (Message) element;
				return msg.getImportance().value();
			}
		});
	}

	private TableViewer createTableViewer(Composite parent) {
		TableViewer tableViewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		tableViewer.addFilter(filter);
		tableViewer.setContentProvider(new ArrayContentProvider());
		getSite().setSelectionProvider(tableViewer);

		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		// Make the selection available to other views
		getSite().setSelectionProvider(tableViewer);
		tableViewer.getControl().setLayoutData(gridData);
		comparator = new TableComparator();
		tableViewer.setComparator(comparator);
		tableViewer.refresh();
		return tableViewer;
	}

	private Search createFindField(Composite parent) {
		Label searchLabel = new Label(parent, SWT.BORDER_SOLID);
		searchLabel.setText("Find: ");
		final Text searchText = new Text(parent, SWT.BORDER | SWT.SEARCH);
		searchText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		searchText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent ke) {
				filter.setSearchQuery(searchText.getText());
				tableViewer.refresh();
			}

		});
		Search filter = new Search();
		return filter;
	}

	/**
	 * Diese Methode erzeugt die Spalten der Table
	 * 
	 * @param String
	 *            title, int bound, int colNumber
	 * @return TableViewerColumn viewerColumn
	 */
	private TableViewerColumn createTableViewerColumn(String title, int bound,
			int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(
				tableViewer, SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		column.addSelectionListener(getSelectionAdapter(column, colNumber));
		column.pack();
		tableViewer.refresh();
		return viewerColumn;
	}

	public void run() {
		ISelection selection = tableViewer.getSelection();
		@SuppressWarnings("unused")
		Object obj = ((IStructuredSelection) selection).getFirstElement();
		// TODO put here selected MailItem name
		ClientManager.getInstance().setSelectedMessage(null);
	}

	private SelectionAdapter getSelectionAdapter(final TableColumn column,
			final int index) {
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				comparator.setColumn(index);
				int dir = tableViewer.getTable().getSortDirection();
				if (tableViewer.getTable().getSortColumn() == column) {
					dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;

				} else {
					dir = SWT.DOWN;
				}
				tableViewer.getTable().setSortDirection(dir);
				tableViewer.getTable().setSortColumn(column);
				tableViewer.refresh();
			}
		};
		return selectionAdapter;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */

	@Override
	public void setFocus() {
		tableViewer.getControl().setFocus();
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof DirectoryChangedEvent) {
			this.currentFolder = ClientManager.getInstance()
					.getSelectedFolder();
			// this.currentFolder = (DirectoryItem) arg1;
			this.messageList = new MailListContentProvider(currentFolder)
					.getMessageContent();
			tableViewer.setInput(messageList);
		}
	}
}
