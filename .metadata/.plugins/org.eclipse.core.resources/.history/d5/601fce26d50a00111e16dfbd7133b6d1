package de.bht.fpa.mail.s750042.navigation;

import java.io.File;
import java.util.ArrayList;

public class DirectoryItem extends AbstractFileSystemItem {

  public DirectoryItem(File file) {
    super(file);
  }

  public boolean hasChildren() {
    File[] children = getFile().listFiles();
    if (children == null) {
      return false;
    }
    for (File subdir : children) {
      if (subdir.isDirectory()) {
        return true;
      }
    }
    return false;
  }

  public DirectoryItem[] getSubdirectories() {
    ArrayList<DirectoryItem> directories = new ArrayList<DirectoryItem>();
    File[] children = getFile().listFiles();
    if (children == null) {
      return directories.toArray(new DirectoryItem[directories.size()]);
    }
    for (File subdir : children) {
      if (subdir.isDirectory()) {
        directories.add(new DirectoryItem(subdir));
      }
    }
    return directories.toArray(new DirectoryItem[directories.size()]);
  }

  public FileItem[] getFiles() {
    ArrayList<FileItem> files = new ArrayList<FileItem>();
    File[] children = getFile().listFiles();
    if (children == null) {
      return null;
    }
    if (children.length == 0) {
      return null;
    }
    for (File file : children) {
      if (!file.isDirectory()) {
        files.add(new FileItem(file));
      }
    }
    return files.toArray(new FileItem[0]);
  }

  @Override
  public String toString() {
    return getName();
  }
}
