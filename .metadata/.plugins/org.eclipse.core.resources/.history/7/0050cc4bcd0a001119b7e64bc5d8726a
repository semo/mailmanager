package de.bht.fpa.mail.s750042.messages;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import de.bht.fpa.mail.s750042.control.ClientManager;
import de.bht.fpa.mail.s750042.navigation.DirectoryItem;
import de.bht.fpa.mail.s750042.toolbox.Search;
import de.bht.fpa.mail.s750042.toolbox.TableComparator;

public class MessagesView extends ViewPart implements Observer {

  public static final String ID = "de.bht.fpa.mail.s750042.messages.MessagesView";

  private TableViewer tableViewer;
  private TableComparator comparator;
  private Search filter;

  @Override
  public void createPartControl(Composite parent) {

    GridLayout layout = new GridLayout(2, false);
    parent.setLayout(layout);

    // START Find Field
    Label searchLabel = new Label(parent, SWT.BORDER_SOLID);
    searchLabel.setText("Find: ");
    final Text searchText = new Text(parent, SWT.BORDER | SWT.SEARCH);
    searchText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
    searchText.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent ke) {
        filter.setSearchQuery(searchText.getText());
        tableViewer.refresh();
      }

    });
    filter = new Search();
    // END Find Field

    // START TableViewer creation
    tableViewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
    String[] titles = { "Von", "Betreff", "Erhalten", "Wichtig" };
    int[] bounds = { 150, 150, 150, 15 };
    tableViewer.addFilter(filter);

    // START Table creation
    final Table table = tableViewer.getTable();
    table.setHeaderVisible(true);
    table.setLinesVisible(true);
    table.setVisible(true);

    tableViewer.setContentProvider(new MailListContentProvider());
    tableViewer.setInput(null);
    tableViewer.refresh();

    // START TableViewerColumn creation
    TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);
    col.setLabelProvider(new ColumnLabelProvider() {
      @Override
      public String getText(Object element) {
        return null;
      }
    });

    col = createTableViewerColumn(titles[1], bounds[1], 1);
    col.setLabelProvider(new ColumnLabelProvider() {
      @Override
      public String getText(Object element) {
        return null;
      }
    });

    col = createTableViewerColumn(titles[2], bounds[2], 2);
    col.setLabelProvider(new ColumnLabelProvider() {
      @Override
      public String getText(Object element) {
        return null;
      }
    });

    col = createTableViewerColumn(titles[3], bounds[3], 3);
    col.setLabelProvider(new ColumnLabelProvider() {
      @Override
      public String getText(Object element) {
        return null;
      }
    });
    // END TableViewer creation

    // Layout the viewer
    GridData gridData = new GridData();
    gridData.verticalAlignment = GridData.FILL;
    gridData.horizontalSpan = 2;
    gridData.grabExcessHorizontalSpace = true;
    gridData.grabExcessVerticalSpace = true;
    gridData.horizontalAlignment = GridData.FILL;
    // Make the selection available to other views
    getSite().setSelectionProvider(tableViewer);
    tableViewer.getControl().setLayoutData(gridData);
    comparator = new TableComparator();
    tableViewer.setComparator(comparator);
    tableViewer.refresh();

    ClientManager.getInstance().addObserver(this);
  }

  /**
   * Diese Methode erzeugt die Spalten der Table
   * 
   * @param String
   *          title, int bound, int colNumber
   * @return TableViewerColumn viewerColumn
   */
  private TableViewerColumn createTableViewerColumn(String title, int bound, int colNumber) {
    final TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
    final TableColumn column = viewerColumn.getColumn();
    column.setText(title);
    column.setWidth(bound);
    column.setResizable(true);
    column.setMoveable(true);
    column.addSelectionListener(getSelectionAdapter(column, colNumber));
    column.pack();
    tableViewer.refresh();
    return viewerColumn;
  }

  public void run() {
    ISelection selection = tableViewer.getSelection();
    @SuppressWarnings("unused")
    Object obj = ((IStructuredSelection) selection).getFirstElement();
    // TODO put here selected MailItem name
    ClientManager.getInstance().setSelectedMessage(null);
  }

  private SelectionAdapter getSelectionAdapter(final TableColumn column, final int index) {
    SelectionAdapter selectionAdapter = new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
        comparator.setColumn(index);
        int dir = tableViewer.getTable().getSortDirection();
        if (tableViewer.getTable().getSortColumn() == column) {
          dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;

        } else {
          dir = SWT.DOWN;
        }
        tableViewer.getTable().setSortDirection(dir);
        tableViewer.getTable().setSortColumn(column);
        tableViewer.refresh();
      }
    };
    return selectionAdapter;
  }

  /**
   * Passing the focus request to the viewer's control.
   */

  @Override
  public void setFocus() {
    tableViewer.getControl().setFocus();
  }

  @Override
  public void update(Observable arg0, Object arg1) {
    if (arg1 instanceof DirectoryItem) {
      System.out.println("WORKS");
    }
  }
}
