package de.bht.fpa.mail.s750042.navigation;

import java.io.File;

/**
 * More concrete implementation of IFileSystem which will be extending its
 * capability to make use of Files and Directories.
 * 
 * @author Sebastian Morkisch
 * 
 */

public abstract class AbstractFileSystemItem implements IFileSystemItem {

  private final File file;

  public AbstractFileSystemItem(File file) {
    this.file = file;
  }

  public File getFile() {
    return file;
  }

  @Override
  public String getName() {
    return file.getName();
  }

}
