package de.bht.fpa.mail.s750042.navigation;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class NavigationViewLabelProvider extends LabelProvider {

  @Override
  public String getText(Object obj) {
    return obj.toString();
  }

  @Override
  public Image getImage(Object obj) {
    String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
    if (obj instanceof DirectoryItem) {
      imageKey = ISharedImages.IMG_OBJ_FOLDER;
    }
    return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
  }
}
