package de.bht.fpa.mail.s750042.navigation;

import java.io.File;

/**
 * Simply keeping a File.
 * 
 * @author Sebastian Morkisch
 * 
 */

public class FileItem extends AbstractFileSystemItem {

  public FileItem(File file) {
    super(file);
  }
}
