package de.bht.fpa.mail.s750042.navigation;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import de.bht.fpa.mail.s750042.control.ClientManager;
import de.bht.fpa.mail.s750042.events.ApplicationWorkspaceChangedEvent;
import de.bht.fpa.mail.s750042.toolbox.Constants;
import de.bht.fpa.mail.s750042.toolbox.SelectionHelper;

public class NavigationView extends ViewPart implements Observer {
  public static final String ID = "de.bht.fpa.mail.s750042.navigation.navigationView";
  private TreeViewer viewer;

  /**
   * Here the Directory Tree will be displayed.
   */
  @Override
  public void createPartControl(Composite parent) {
    viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
    viewer.setContentProvider(new NavigationViewContentProvider());
    viewer.setLabelProvider(new NavigationViewLabelProvider());
    viewer.setInput(getRootDirectory());
    viewer.addSelectionChangedListener(new ISelectionChangedListener() {
      @Override
      public void selectionChanged(SelectionChangedEvent event) {
        DirectoryItem directoryItem = SelectionHelper.handleSelectionEvent(event);
        if (directoryItem == null) {
          return;
        }

        ClientManager.getInstance().setSelectedFolder(directoryItem);
        ClientManager.getInstance().sendStatusLineMessage(directoryItem.getName());
      }
    });
    ClientManager.getInstance().addObserver(this);
  }

  /*
   * After closing of the view, the Observer will be diconnected from the
   * ClientManager.
   * 
   * @see org.eclipse.ui.part.WorkbenchPart#dispose()
   */
  @Override
  public void dispose() {
    ClientManager.getInstance().deleteObserver(this);
    super.dispose();
  }

  /**
   * Passing the focus request to the viewer's control.
   */
  @Override
  public void setFocus() {
    viewer.getControl().setFocus();
  }

  @Override
  public void update(Observable o, Object obj) {
    if (obj instanceof ApplicationWorkspaceChangedEvent) {
      viewer.setInput(((ApplicationWorkspaceChangedEvent) obj).getWorkspaceHome());
    }
  }

  private DirectoryItem getRootDirectory() {
    return new DirectoryItem(new File(Constants.USER_HOME));
  }
}