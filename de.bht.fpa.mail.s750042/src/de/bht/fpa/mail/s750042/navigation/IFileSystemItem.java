package de.bht.fpa.mail.s750042.navigation;

/**
 * This is the Base of all Mails which will later be filtered out from the
 * filesystem.
 * 
 * @author Sebastian Morkisch
 * 
 */

public interface IFileSystemItem {

  String getName();

}
