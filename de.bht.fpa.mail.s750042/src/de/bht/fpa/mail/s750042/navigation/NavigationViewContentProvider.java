package de.bht.fpa.mail.s750042.navigation;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class NavigationViewContentProvider implements IStructuredContentProvider, ITreeContentProvider {

  @Override
  public void dispose() {
  }

  @Override
  public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
  }

  @Override
  public Object[] getChildren(Object parent) {
    if (parent instanceof DirectoryItem) {
      return ((DirectoryItem) parent).getSubdirectories();
    }
    return new Object[0];
  }

  @Override
  public Object getParent(Object child) {
    if (child instanceof DirectoryItem) {
      return child;
    }
    return null;
  }

  @Override
  public boolean hasChildren(Object parent) {
    if (parent instanceof DirectoryItem) {
      return ((DirectoryItem) parent).hasChildren();
    }
    return false;
  }

  @Override
  public Object[] getElements(Object parent) {
    if (parent instanceof DirectoryItem) {
      return ((DirectoryItem) parent).getSubdirectories();
    }
    return new Object[0];
  }
}