package de.bht.fpa.mail.s750042;

import java.io.File;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.bht.fpa.mail.s750042.control.ClientManager;
import de.bht.fpa.mail.s750042.navigation.DirectoryItem;

public class Settings extends Action {

  @SuppressWarnings("unused")
  private final String viewId;
  @SuppressWarnings("unused")
  private final IWorkbenchWindow window;

  public Settings(IWorkbenchWindow window, String label, String viewId) {
    this.window = window;
    this.viewId = viewId;
    setText(label);
    // The id is used to refer to the action in a menu or toolbar
    setId(ICommandIds.CMD_OPEN);
    // Associate the action with a pre-defined command, to allow key
    // bindings.
    setActionDefinitionId(ICommandIds.CMD_OPEN);
    setImageDescriptor(Activator.getImageDescriptor("/icons/sample2.gif"));
  }

  @Override
  public void run() {
    Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
    DirectoryDialog navigateFileSystemObject = new DirectoryDialog(shell);

    /**
     * Does the Base Directory issued by a simple FileChooser.
     */

    navigateFileSystemObject.setMessage("Set Base Directory");
    String selectedDir = navigateFileSystemObject.open();

    if (selectedDir != null) {
      File file = new File(selectedDir);
      DirectoryItem newBaseDir = new DirectoryItem(file);
      ClientManager.getInstance().setApplicationWorkspace(newBaseDir);
    }
  }
}
