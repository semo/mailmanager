package de.bht.fpa.mail.s750042;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import de.bht.fpa.mail.s750042.control.ClientManager;
import de.bht.fpa.mail.s750042.toolbox.Constants;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

  private String statusLineString;

  public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
    super(configurer);
  }

  @Override
  public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
    return new ApplicationActionBarAdvisor(configurer);
  }

  @Override
  public void preWindowOpen() {
    IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
    configurer.setInitialSize(new Point(Constants.APP_WIDTH, Constants.APP_HEIGHT));
    configurer.setShowCoolBar(true);
    configurer.setShowStatusLine(true);
  }

  @Override
  public void postWindowOpen() {
    IStatusLineManager statusMessage = getWindowConfigurer().getActionBarConfigurer().getStatusLineManager();
    statusMessage.setMessage(null, statusLineString);
    ClientManager.getInstance().setStatusLineMessage(statusMessage);
  }

}