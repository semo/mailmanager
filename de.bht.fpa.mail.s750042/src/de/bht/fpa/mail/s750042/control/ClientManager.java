package de.bht.fpa.mail.s750042.control;

import java.util.Observable;

import org.eclipse.jface.action.IStatusLineManager;

import de.bht.fpa.mail.common.model.Message;
import de.bht.fpa.mail.s750042.events.ApplicationWorkspaceChangedEvent;
import de.bht.fpa.mail.s750042.events.DirectoryChangedEvent;
import de.bht.fpa.mail.s750042.events.MailChangedEvent;
import de.bht.fpa.mail.s750042.navigation.DirectoryItem;

/**
 * This class is a kind of a Heart of the Application which beats whenever a
 * User-made event has to be distributed to its limbs such as the
 * NavigationView, MessageView, MessageListView. For this purpose this class
 * implements the Observable Interface.
 * 
 * @author Sebastian Morkisch
 * 
 */

public class ClientManager extends Observable {

  private static ClientManager instance = null;
  private IStatusLineManager statusLine;
  private DirectoryItem dirItem;
  private Message selMsg;

  public static synchronized ClientManager getInstance() {
    if (instance == null) {
      instance = new ClientManager();
    }
    return instance;
  }

  /**
   * Notified if workspace of the Client has changed
   * 
   * @param DirectoryItem
   *          newBaseDir Workspace of the Application
   */
  public void setApplicationWorkspace(DirectoryItem newBaseDir) {
    setChanged();
    notifyObservers(new ApplicationWorkspaceChangedEvent(newBaseDir));
  }

  public void setStatusLineMessage(IStatusLineManager statusMessage) {
    this.statusLine = statusMessage;
  }

  /**
   * Passes the message Object to the IStatuslineManager.setMessage(String)
   * 
   * @param message
   */
  public void sendStatusLineMessage(String message) {
    if (message != null) {
      statusLine.setMessage(message);
    }
  }

  public void setSelectedFolder(DirectoryItem selectedDirectoryItem) {
    if (selectedDirectoryItem != null) {
      this.dirItem = selectedDirectoryItem;
      setChanged();
      notifyObservers(new DirectoryChangedEvent(selectedDirectoryItem));
    }
  }

  public void setSelectedMessage(Message selectedMessage) {
    if (selectedMessage != null) {
      this.selMsg = selectedMessage;
      setChanged();
      notifyObservers(new MailChangedEvent(selectedMessage));
    }
  }

  public Message getSelectedMessage() {
    return selMsg;
  }

  public DirectoryItem getSelectedFolder() {
    return dirItem;
  }
}