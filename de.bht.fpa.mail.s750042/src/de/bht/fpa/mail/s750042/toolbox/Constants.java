package de.bht.fpa.mail.s750042.toolbox;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.graphics.Font;

public final class Constants {

  protected Constants() {
    // prevents calls from subclass
    throw new UnsupportedOperationException();
  }

  public static final String USER_HOME = "user.home";

  public static final int APP_WIDTH = 800; // initial window width after startup
  public static final int APP_HEIGHT = 600; // initial window height after
                                            // startup

  public static final int BORDER_MARGIN_WIDTH = 10;
  public static final int BORDER_MARGIN_HEIGHT = 5;

  public static final float VIEW_SEPARATION_RATIO = 0.25f;
  public static final float ICON_MARGIN = 0.5f;

  public static final int INDEX_DATE = 3;
  public static final int INDEX_FROM = 0;
  public static final int INDEX_SUBJECT = 1;
  public static final int COLUMN_WIDTH = 250;
  public static final int DESCENDING = 1; // targets ordering column cell
                                          // content

  public static final String LABEL_FROM = "Von: ";
  public static final String LABEL_DATE = "Erhalten: ";
  public static final Font FAT_FONT = JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT);

  public static final String LABEL_SUBJECT = "Betreff: ";

}
