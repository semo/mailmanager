package de.bht.fpa.mail.s750042.toolbox;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import de.bht.fpa.mail.common.model.Message;

/**
 * Makes functionality available for the Search Field in the Client.
 * 
 * @author Sebastian Morkisch
 * 
 */

public class Search extends ViewerFilter {

  private String query;

  /**
   * To make a better look and feel for users I use toLowerCase.
   * 
   * @param query
   */

  public void setSearchQuery(String query) {
    this.query = (".*" + query + ".*").toLowerCase();
  }

  @Override
  public boolean select(Viewer viewer, Object parentElement, Object element) {
    if (query == null || query.length() == 0) {
      return true;
    }

    if (((Message) element).getSender().getFirstName().matches(query)) {
      return true;
    }
    if (((Message) element).getSender().getName().matches(query)) {
      return true;
    }
    if (((Message) element).getSender().getEmail().matches(query)) {
      return true;
    }

    if (((Message) element).getRecipient().getEmail().matches(query)) {
      return true;
    }
    if (((Message) element).getRecipient().getName().matches(query)) {
      return true;
    }
    if (((Message) element).getRecipient().getEmail().matches(query)) {
      return true;
    }

    if (((Message) element).getReceived().toString().matches(query)) {
      return true;
    }

    return false;
  }

}
