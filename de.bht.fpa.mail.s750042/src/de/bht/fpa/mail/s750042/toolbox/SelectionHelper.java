package de.bht.fpa.mail.s750042.toolbox;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;

/**
 * This class is made to filter out NULLZ and returns from a selection the
 * element. It was a evolutionary buddy of NavigationView.
 * 
 * @author Siamak Haschemi
 * 
 */

public final class SelectionHelper {

  @SuppressWarnings("unchecked")
  public static <T> T handleSelectionEvent(SelectionChangedEvent event) {
    if (event == null) {
      return null;
    }
  
    ISelection selection = event.getSelection();
  
    if (selection == null || !(selection instanceof IStructuredSelection)) {
      return null;
    }
    IStructuredSelection structuredSelection = (IStructuredSelection) selection;
    Object firstElement = structuredSelection.getFirstElement();
  
    if (firstElement == null) {
      return null;
    }
    return (T) firstElement;
  }

  private SelectionHelper() {
  }
}
