package de.bht.fpa.mail.s750042.toolbox;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import de.bht.fpa.mail.common.model.Message;

public class TableComparator extends ViewerComparator {

  private int propertyIndex;
  private int direction = Constants.DESCENDING;

  public TableComparator() {
    this.propertyIndex = 0;
    direction = Constants.DESCENDING;
  }

  public int getPropertyIndex() {
    return propertyIndex;
  }

  public void setColumn(int column) {
    if (column == this.propertyIndex) {
      direction = 1 - direction;
    } else {
      this.propertyIndex = column;
      direction = Constants.DESCENDING;
    }
  }

  @Override
  public int compare(Viewer viewer, Object e1, Object e2) {
    Message m1 = (Message) e1;
    Message m2 = (Message) e2;

    // switch (propertyIndex) {
    // case Constants.INDEX_SUBJECT:
    //
    // break;
    // case Constants.INDEX_FROM:
    //
    // break;
    //
    // default:
    // break;
    // }
    return m1.getSender().toString().compareTo(m2.getSender().toString());
  }

}
