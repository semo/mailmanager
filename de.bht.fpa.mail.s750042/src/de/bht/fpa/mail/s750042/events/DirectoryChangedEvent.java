package de.bht.fpa.mail.s750042.events;

import de.bht.fpa.mail.s750042.navigation.DirectoryItem;

public class DirectoryChangedEvent {

  private final DirectoryItem selectedDirectory;

  public DirectoryChangedEvent(DirectoryItem selectedDir) {
    this.selectedDirectory = selectedDir;
  }

  public DirectoryItem getSelectedFolder() {
    return selectedDirectory;
  }
}
