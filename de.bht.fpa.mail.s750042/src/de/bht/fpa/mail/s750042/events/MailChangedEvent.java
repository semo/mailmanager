package de.bht.fpa.mail.s750042.events;

import de.bht.fpa.mail.common.model.Message;

/**
 * This class is pretty much a helper for the Coder to find out which kind of
 * event was sent. Perhaps it would've be possible to make this class as well as
 * ApplicationWorkspaceChangedEvent and DirectoryChangedEvent unused by setting
 * up more and differenciated observers.
 */

public class MailChangedEvent {

  private final Message selectedMessage;

  public MailChangedEvent(Message message) {
    this.selectedMessage = message;
  }

  public Message getSelectedMessage() {
    return selectedMessage;
  }

}
