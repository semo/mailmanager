package de.bht.fpa.mail.s750042.events;

import de.bht.fpa.mail.s750042.navigation.DirectoryItem;

public class ApplicationWorkspaceChangedEvent {

  private final DirectoryItem workspaceHome;

  public ApplicationWorkspaceChangedEvent(DirectoryItem workspaceHome) {
    this.workspaceHome = workspaceHome;
  }

  public DirectoryItem getWorkspaceHome() {
    return workspaceHome;
  }

}
