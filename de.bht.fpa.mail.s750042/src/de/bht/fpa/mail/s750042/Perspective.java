package de.bht.fpa.mail.s750042;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.bht.fpa.mail.s750042.messages.MessageView;
import de.bht.fpa.mail.s750042.messages.MessagesListView;
import de.bht.fpa.mail.s750042.navigation.NavigationView;
import de.bht.fpa.mail.s750042.toolbox.Constants;

public class Perspective implements IPerspectiveFactory {

  /**
   * The ID of the perspective as specified in the extension.
   */
  public static final String ID = "de.bht.fpa.mail.s750042.perspective";

  @Override
  public void createInitialLayout(IPageLayout layout) {
    String editorArea = layout.getEditorArea();
    layout.setEditorAreaVisible(false);

    layout.addStandaloneView(NavigationView.ID, false, IPageLayout.LEFT, Constants.VIEW_SEPARATION_RATIO, editorArea);
    layout.addStandaloneView(MessagesListView.ID, false, IPageLayout.TOP, Constants.VIEW_SEPARATION_RATIO, editorArea);
    layout.addStandaloneView(MessageView.ID, false, IPageLayout.BOTTOM, Constants.VIEW_SEPARATION_RATIO, editorArea);

    layout.getViewLayout(NavigationView.ID).setCloseable(true);
    layout.getViewLayout(NavigationView.ID).setMoveable(true);
    layout.getViewLayout(MessagesListView.ID).setCloseable(true);
    layout.getViewLayout(MessagesListView.ID).setMoveable(true);
    layout.getViewLayout(MessageView.ID).setCloseable(true);
    layout.getViewLayout(MessageView.ID).setMoveable(true);
  }
}
