package de.bht.fpa.mail.s750042.messages;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import de.bht.fpa.mail.common.model.Message;
import de.bht.fpa.mail.s750042.navigation.DirectoryItem;
import de.bht.fpa.mail.s750042.navigation.FileItem;

public class MailListContentProvider {

  private ArrayList<Message> messages;
  private JAXBContext context;
  private Unmarshaller uMarsh;
  private final DirectoryItem messageFolder;

  public MailListContentProvider(DirectoryItem dirItem) {
    this.messageFolder = dirItem;
  }

  /**
   * To unmarshal all Mails, the Common Project of my Prof is used to unwrap
   * Mails to Messages. Therefore a List will be returned. But before the Files
   * of a folder will be filtered by some handy Methods below.
   * 
   * @return
   */

  public List<Message> getMessageContent() {
    messages = new ArrayList<Message>();
    if (getMessageFolder().getFiles() == null) {
      return messages;
    }

    ArrayList<FileItem> filesUnfiltered = getMessageFolder().getFiles();
    ArrayList<FileItem> filesFiltered = new ArrayList<FileItem>();

    for (FileItem fileItem : filesUnfiltered) {
      if (acceptXML(fileItem)) {
        filesFiltered.add(fileItem);
      }
    }

    try {
      context = JAXBContext.newInstance(Message.class);
      uMarsh = context.createUnmarshaller();

      for (FileItem fileItem : filesFiltered) {
        messages.add((Message) uMarsh.unmarshal(new File(fileItem.getFile().getAbsolutePath())));
      }
    } catch (JAXBException e) {
      return messages;
    }
    return messages;
  }

  @Override
  public String toString() {
    return "MailListContentProvider [messages=" + messages + "]";
  }

  private static String getExtension(FileItem fi) {
    String extension = null;
    String s = fi.getName();
    int i = s.lastIndexOf('.');

    if (i > 0 && i < s.length() - 1) {
      extension = s.substring(i + 1).toLowerCase();
    }
    return extension;
  }

  private boolean acceptXML(FileItem fi) {
    String extension = getExtension(fi);
    if (extension != null) {
      if (extension.equals("xml")) {
        return true;
      }
    }
    return false;
  }

  private DirectoryItem getMessageFolder() {
    return messageFolder;
  }

}
