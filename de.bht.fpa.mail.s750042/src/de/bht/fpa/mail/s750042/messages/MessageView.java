package de.bht.fpa.mail.s750042.messages;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import de.bht.fpa.mail.common.model.Message;
import de.bht.fpa.mail.s750042.control.ClientManager;
import de.bht.fpa.mail.s750042.events.MailChangedEvent;
import de.bht.fpa.mail.s750042.toolbox.Constants;

public class MessageView extends ViewPart implements Observer {

  public static final String ID = "de.bht.fpa.mail.s750042.messages.MessageView";

  private Composite banner;
  private Composite compositeParent;

  private Message selectedMessage;

  private Label senderLabelStatic;
  private Label subjectLabelStatic;
  private Label dateLabelStatic;

  private Label senderLabelDynamic;
  private Label subjectLabelDynamic;
  private Label dateLabelDynamic;
  private Text textLabelDynamic;

  @Override
  public void createPartControl(Composite parent) {
    this.compositeParent = configureMessageView(parent);
    this.banner = configureBannerView(compositeParent);

    this.senderLabelStatic = new Label(banner, SWT.WRAP);
    this.senderLabelStatic.setText(Constants.LABEL_FROM);
    this.senderLabelStatic.setFont(Constants.FAT_FONT);
    this.senderLabelDynamic = new Label(banner, SWT.WRAP);

    this.dateLabelStatic = new Label(banner, SWT.WRAP);
    this.dateLabelStatic.setText(Constants.LABEL_DATE);
    this.dateLabelStatic.setFont(Constants.FAT_FONT);
    this.dateLabelDynamic = new Label(banner, SWT.WRAP);

    this.subjectLabelStatic = new Label(banner, SWT.WRAP);
    this.subjectLabelStatic.setText(Constants.LABEL_FROM);
    this.subjectLabelStatic.setFont(Constants.FAT_FONT);
    this.subjectLabelDynamic = new Label(banner, SWT.WRAP);

    this.textLabelDynamic = new Text(compositeParent, SWT.MULTI | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL);
    textLabelDynamic.setLayoutData(new GridData(GridData.FILL_BOTH));

    ClientManager.getInstance().addObserver(this);
  }

  @Override
  public void update(Observable arg0, Object arg1) {
    if (arg1 instanceof MailChangedEvent) {
      setSelectedMessage(ClientManager.getInstance().getSelectedMessage());
      updateMessageViewContent();
      ClientManager.getInstance().sendStatusLineMessage(
          "Message opened: " + ClientManager.getInstance().getSelectedMessage().getSender().getFirstName() + " "
              + ClientManager.getInstance().getSelectedMessage().getSender().getName());
    }
  }

  @Override
  public void setFocus() {
  }

  @Override
  public void dispose() {
    ClientManager.getInstance().deleteObserver(this);
    super.dispose();
  }

  private void getSubjectFromMessage(Composite banner) {
    try {
      subjectLabelDynamic.setText(getSelectedMessage().getSubject());
    } catch (IllegalArgumentException e) {
      subjectLabelDynamic.setText("");
    }
    subjectLabelDynamic.pack();
  }

  private void getDateFromMessage(Composite banner) {
    try {
      dateLabelDynamic.setText(getSelectedMessage().getReceived().toString());
    } catch (IllegalArgumentException e) {
      dateLabelDynamic.setText("");
    }
    dateLabelDynamic.pack();
  }

  private void getTextFromMessage(Composite compositeParent) {
    try {
      textLabelDynamic.setText(getSelectedMessage().getText());
      textLabelDynamic.setEditable(false);
    } catch (IllegalArgumentException e) {
      textLabelDynamic.setText("");
    }
    textLabelDynamic.update();
  }

  /**
   * Uses the Messages information to be displayed in the banner of the
   * MessageView. If some of the information is n/a then a String Buffer will
   * use the information available.
   * 
   * @param banner
   */
  private void getSenderFromMessage(Composite banner) {
    StringBuilder stribu = new StringBuilder();

    if (getSelectedMessage().getSender().getFirstName() != null) {
      stribu.append(getSelectedMessage().getSender().getFirstName() + " ");
    }
    if (getSelectedMessage().getSender().getName() != null) {
      stribu.append(getSelectedMessage().getSender().getName() + " ");
    }
    if (getSelectedMessage().getSender().getEmail() != null) {
      stribu.append("<" + getSelectedMessage().getSender().getEmail() + ">");
    }
    senderLabelDynamic.setText(stribu.toString());
    senderLabelDynamic.pack();
  }

  private Composite configureBannerView(Composite top) {
    GridLayout layout;
    banner = new Composite(top, SWT.NONE);
    banner.setLayoutData(new GridData(SWT.FILL, GridData.VERTICAL_ALIGN_BEGINNING, true, false));
    layout = new GridLayout();
    layout.marginHeight = Constants.BORDER_MARGIN_HEIGHT;
    layout.marginWidth = Constants.BORDER_MARGIN_WIDTH;
    layout.numColumns = 2;
    banner.setLayout(layout);
    return banner;
  }

  private Composite configureMessageView(Composite parent) {
    compositeParent = new Composite(parent, SWT.NONE);
    GridLayout layout = new GridLayout();
    layout.marginHeight = 0;
    layout.marginWidth = 0;
    compositeParent.setLayout(layout);
    return compositeParent;
  }

  private void updateMessageViewContent() {
    getSenderFromMessage(banner);
    getDateFromMessage(banner);
    getSubjectFromMessage(banner);
    getTextFromMessage(compositeParent);
  }

  private Message getSelectedMessage() {
    return selectedMessage;
  }

  private void setSelectedMessage(Message selectedMessage) {
    this.selectedMessage = selectedMessage;
  }
}