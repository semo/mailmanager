package de.bht.fpa.mail.common.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Message {
  @Id
  private String id;
  private Sender sender;
  private Recipient recipient;
  private String text;
  private String subject;
  private Importance importance;

  @Temporal(TemporalType.DATE)
  private Date received;

  @ElementCollection
  private List<Attachment> attachment = new LinkedList<Attachment>();

  @Column(name = "READ_")
  private boolean read;

  public String getId() {
	 
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Sender getSender() {
    return sender;
  }

  public void setSender(Sender sender) {
    this.sender = sender;
  }

  public Recipient getRecipient() {
    return recipient;
  }

  public void setRecipient(Recipient recipient) {
    this.recipient = recipient;
  }

  @XmlJavaTypeAdapter(DateAdapter.class)
  public Date getReceived() {
    return received;
  }

  public void setReceived(Date received) {
    this.received = received;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public List<Attachment> getAttachment() {
    return attachment;
  }

  public void setAttachment(List<Attachment> attachment) {
    this.attachment = attachment;
  }

  public Importance getImportance() {
    return importance;
  }

  public void setImportance(Importance importance) {
    this.importance = importance;
  }

  public boolean isRead() {
    return read;
  }

  public void setRead(boolean read) {
    this.read = read;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    s.append("[Message: ");
    s.append("id=").append(id).append(" ");
    s.append("sender=").append(sender).append(" ");
    s.append("recipient=").append(recipient).append(" ");
    s.append("received=").append(received).append(" ");
    s.append("subject=").append(subject).append(" ");
    s.append("read=").append(read).append(" ");
    s.append("text=").append(text).append(" ");
    s.append("importance=").append(importance).append(" ");
    s.append("attachment=(");
    for (Attachment a : attachment) {
      s.append(a).append(",");
    }
    s.append(")");
    s.append("]");
    return s.toString();
  }
}
