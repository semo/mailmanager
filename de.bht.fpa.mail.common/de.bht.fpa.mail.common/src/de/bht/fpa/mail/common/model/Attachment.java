package de.bht.fpa.mail.common.model;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlRootElement;

@Embeddable
@XmlRootElement
public class Attachment {
  private String fileName;
  private String body;

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    s.append("[Attachment: ");
    s.append("fileName=").append(fileName).append(" ");
    s.append("body=").append(body);
    s.append("]");
    return s.toString();
  }
}
