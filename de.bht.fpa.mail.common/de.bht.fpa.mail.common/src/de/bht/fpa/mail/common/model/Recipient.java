package de.bht.fpa.mail.common.model;

import java.io.Serializable;

public class Recipient implements Serializable {
  private static final long serialVersionUID = -2628672213043973675L;

  private String email;
  private String firstName;
  private String name;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    s.append("[Recipient: ");
    s.append("email=").append(email).append(" ");
    s.append("firstName=").append(firstName).append(" ");
    s.append("name=").append(name);
    s.append("]");
    return s.toString();
  }

}
